---
id: setup
title: Setting up the Alexa Skill
sidebar_label: Setup
---

This section of the guide is sequential in nature. While each step is self-contained, they should be completed in order to avoid unnecessary complications.

## Installing the Repository

```bash
# Clone the Git repository
git clone https://github.com/elasticpath/alexa-skill.git

# Go into the cloned directory
cd alexa-skill/lambda/main

# Install all the dependencies for the project
npm install  --production
```

## Deploying the Skill to AVS (Alexa Voice Services)

1. In the Alexa Developer Console, create a `Custom, Self-Hosted` skill
2. Select the **Start From Scratch** template
3. In the **Interaction Model**, select the JSON Editor
4. Drag the `models/en-US.json` file into the JSON Editor
5. Build the Model

## Setting up the Lambda

1. Zip the contents of the `alexa-skill/lambda/main` directory, including the `node_modules` folder
2. In AWS (Amazon Web Services), create a Lambda function with an Alexa Skills Kit trigger.

    **Note**: Currently, only the following AWS regions support Alexa Skills Kit triggers:

    * EU (Ireland)
    * US East (N. Virginia)
    * US West (Oregon)

3. Enter the Skill ID from the Alexa Skill configured in [Deploying the the Skill to AVS](#deploying-the-skill-to-avs-alexa-voice-services) in the Configure Triggers input (This can be retrieved through the Alexa Developer Console)
4. Upload the zip file you created to your Lambda
5. Create an environment variable called `CORTEX_URL` that points to a publicly available Cortex endpoint
6. Save the function

## Deploying the Skill to AVS (Alexa Voice Services) - Part 2

1. Return to the Alexa Developer Console, and navigate to the skill you created in [Deploying the the Skill to AVS](#deploying-the-skill-to-avs-alexa-voice-services)
2. Select the **Endpoint** link
3. Enter the ARN (Amazon Resource Names) for the Lambda you created in the [Setting up the Lambda](#setting-up-the-lambda) section, or an HTTPS endpoint where you host your skill
4. Save the Endpoints
5. In the header menu, select **Test**
6. In the *Skill testing is enabled in* field, select **Development**
7. You can now begin testing

## Setting up a Development Environment

If you want to customize the skill, enable local development by using the [alexa-skill-local](https://www.npmjs.com/package/alexa-skill-local) module.

### Before You Begin

Complete the [Deploying the Skill to AVS](#deploying-the-skill-to-avs-alexa-voice-services) section before you develop locally

### Procedure

1. Modify the `asl-config.json` file, replacing the placeholder `skillId` value with your Alexa Skill ID
2. Run the `npm install` command to install depenedencies
3. Create an environment variable called `CORTEX_URL` that points to a publicly available Cortex endpoint, or a valid Elastic Path development environment
4. For more information, see [The Starting Construction Guide](https://developers.elasticpath.com/commerce/construction-home)
5. Run the `npm start` command to start the `alexa-skill-local` server
6. In a browser, go to `localhost:3001` to link your local development environment to your Amazon Skill
    **Note**: Note the `ngrok` URL from the command line output
7. Go to the Endpoint link under your Alexa skill
8. Select HTTPS enpoint
9. Enter the URL from step 7
10. Save Endpoints
11. New sessions are directed to your local skill. Changes are hotswapped, and local Node debuggers can be attached

## Setting up Account Linking

You may now proceed to setup the login server as described [here](./login-server.html).
