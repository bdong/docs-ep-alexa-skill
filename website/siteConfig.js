// See https://docusaurus.io/docs/site-config for all the possible site configuration options.

const projectRepo = "https://github.com/elasticpath/alexa-skill"
const docRepo = "https://gitlab.com/elasticpath/reference-experience/docs-ep-alexa-skill" ;
const args = process.argv.slice(2);
const siteUrl = args[0] ? "/" + args[0] + "/" : '/alexa-skill/';

const siteConfig = {
  title: 'Alexa Skill', // Title for your website.
  tagline: 'Documentation for Reference Alexa Skill',
  url: 'https://documentation.elasticpath.com', // Your website URL
  baseUrl: siteUrl, // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  projectName: 'docs-ep-alexa-skill',
  organizationName: 'elasticpath',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    { href: projectRepo, label: 'Source', external: true }
  ],

  //Algolia search configuration
  algolia: {
    apiKey: 'de5d327a89e366ea5ecd533b1b93aaea',
    indexName: 'elasticpath_alexa-skill',
    placeholder: 'Search these docs',
  },

  /* path to images for header/footer */
  favicon: 'img/favicon/ep-logo.png',

  /* Colors for website */
  colors: {
    primaryColor: '#0033cc',
    secondaryColor: '#ea7317',
  },

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} Elastic Path, Inc.`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: [
    'https://buttons.github.io/buttons.js',
    'https://documentation.elasticpath.com/_lib/ep-webcomponent/webcomponents-loader.js',
    'https://documentation.elasticpath.com/_lib/ep-webcomponent/public/EpSiteHeader.js',
    'https://documentation.elasticpath.com/_lib/ep-webcomponent/public/EpSiteFooter.js',
    siteUrl + "js/ep-inject-global-header.js"
  ],

  markdownPlugins: [
    // Highlight admonitions.
    require('remarkable-admonitions')({ icon: 'svg-inline' })
  ],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  docsSideNavCollapsible: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/ep-logo-stacked-black.png',
  twitterImage: 'img/ep-logo-stacked-black.png',
  twitterUsername: 'elasticpath',

  // Show documentation's last update time.
  enableUpdateTime: true,
  editUrl: docRepo + '/edit/master/docs/',

  //Enable scrolling to top button
  scrollToTop: true,
};

module.exports = siteConfig;
