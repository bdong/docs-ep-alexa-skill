# Documentation

This repository contains documentation of *<<project-name>>*. Documentation is built and published from this repository and is available at [Elastic Path Documentation site][doc-site].

## Table of content

1. [Quick Start](#quick-start)
1. [Contributing](#contributing)
1. [Usage](#usage)
    1. [Overview of the repository structure](#overview-of-the-repository-structure)
    1. [Prerequisites](#prerequisites)
    1. [Build & Test using Gradle](#using-gradle)
    1. [Build & Test using Yarn](#using-yarn)

---

## Quick Start

It's very easy to get started with this repository. Besides Git, all you need is Java and you're set. To get started:

1. Open a terminal or command prompt from your machine and go to a directory or location where you'd like to clone this repository
1. Clone this repository: `git clone <ssh-or-https-url-of-this-repo>`
1. Go to the `root` directory of this repo: `cd docs-repo-name/root`
1. Run `./gradlew startDevServer`
    - If you don't already have the required Gradle version, it will be downloaded automatically
    - It will also download and setup NodeJS, Yarn, npm packages, etc. as needed by this repository

Your default browser should open automatically with the site running on your local machine. If not, you can access it at http://localhost:3000/template-site/

Let's do a quick test

1. Click on the *Documentation* link from the navigation bar at the top
1. Edit some content of the file `docs/index.md` and save
1. Your changes should now appear on the browser

Great!! Now you know how to start contributing to the docs :). To stop your dev server, in your terminal/command prompt press *control + C* or *CTRL + C*.

## Contributing

In order to contribute to this repository, please follow these steps:

1. Ensure that you've clone the git repository to your local machine
1. Create a new branch from the latest `master` branch
2. Commit your changes
    - Follow the [instructions in usage section](#usage)
    - Be sure to validate your change by running the dev server on your local machine and also running `./gradlew test`. For more details, see [build & test](#build--test)
3. Push your changes upstream and submit a Pull Request for review
    - If other changes have been merged into `master` branch in the meantime, please rebase your branch on the latest of the `master` branch


## Usage

### Overview of the repository structure

The structure this repository should look like this:

```
.
├── README.md
├── docs
│   ├── assets
│   │   ├── ...
│   ├── deployment
│   │   ├── deploy.md
│   │   ├── index.md
│   ├── glossary.md
│   ├── index.md
│   ├── release-notes.md
├── root
└── website
    ├── build.gradle
    ├── package.json
    ├── sidebars.json
    ├── siteConfig.js
    ├── versioned_docs
    │   └── version-1.0.0
    ├── versioned_sidebars
    │   └── version-1.0.0-sidebars.json
    ├── versions.json
```

#### `docs` directory

This directory contains all the documentation for the **Next** release. If there is no versioning involved, then it represents the **current/latest** release docs. In the example above:

- there's a directory named `assets`, which holds all images that may need to be refered to from a documentation
- documents are structured in a logical manner such as release notes, glossary, etc are at the root and all *deployment* related docs are in `deployment` directory

#### `root` directory

This directory holds the main Gradle script and configuration, which can be used for build and test either in your local environment or in CI environment.

#### `website` directory

This directory holds all the configuration of *Docusaurus*, which is used as the tool for building and managing documentation. Some of the key files and directories are:

- `build.gradle`: Gradle build script
- `package.json`: All NPM packages used by this repository
- `sidebars.json`: Configuration file for sidebars of all documents for **Next** release if versioning is used; otherwise it's used for current release
- `siteConfig.json`: Main configuration file for Docusaurus
- `versioned_docs`: If versioning is used, all of the versions will be in their own directory with corresponding version number
- `versioned_sidebars`: This directory contains sidebars for all the releases in their own file with corresponding version number
- `versions.json`: A configuration file listing all the versions

### Prerequisites

The only required tool to work with this repository is *Java* because *Gradle* is used as the build tool. And *Gradle* itself will be downloaded to your home directory and configured automatically, if you don't already have the required version.

You'll also need a text editor (i.e Visual Studio Code, Atom, etc.). Once the repository has been cloned to your local directory, open the directory in your favourite text editor.


#### Why Gradle?

The documentation website is built using [Static Site Generator named Docusaurus][docusaurus-site] which is built using React. That means you need *NodeJS*, and *npm*/*yarn* for building & testing the site. The *Gradle build* in this repository has been configured so that these tools will be automatically downloaded to your computer and will be used for build & test. It will not be installed globally and even if you have the same tools installed already, there will not be any conflict. In summary, it provides the following benefits:

- Be ready to contribute to this repository quickly and easily
- No need to worry about managing different versions of required tools (i.e. NodeJS, NPM, Yarn) because other projects may need the same tool but a different version
- Easy maintenance: If the prerequisites are updated in the code of this repo, they will be reflected automatically on your computer, when you want to contribute changes

#### I don't want to use Gradle

If you don't want to use Gradle, please make sure you have the following tools installed:

- NodeJS (version 10.14.2)
- Yarn (version 1.13.0)

For consistency, please use Yarn instead of NPM


### Build & Test

#### Using Gradle

From your terminal/command prompt, you will need to be in the `root` directory to execute any commands.

- Go to the `root` directory of this repo: `cd root`
- Run `./gradlew clean build` to build the site using a clean environment
    - Built artifats (static site html) will be available in `website/build` directory of this repository
- Run `./gradlew test` to execute all the tests setup for validating documentation guidelines and styles

Refer to the [list of commands](#list-of-all-relevant-commands) for details of various commands that can be executed.

#### Using Yarn

From your terminal/command prompt, you will need to be in the `website` directory to execute any commands. It's recommended to use Gradle but if you don't want to use Gradle, please make sure:

- Use `yarn` instead of `npm`
- Install the [required versions](#i-dont-want-to-use-gradle)
- From your terminal, go to the `website` directory of this repo: `cd website`
- Run `yarn install` to install necessary dependent packages
- Run `yarn clean` to remove build artifacts
- Run `yarn build` to build the site
    - Built artifats (static site html) will be available in `website/build` directory of this repository
- Run `yarn test` to execute all the tests setup for validating documentation guidelines and styles

Refer to the [list of commands](#list-of-all-relevant-commands) for details of various commands that can be executed.


#### List of all relevant commands

- `startDevServer`: Starts the dev server available with *Docusaurus*. This can be used for checking documentation presentation/rendering before submitting for review
- `clean`: Deletes `build` directory and any other temporary directory created during build
- `build`: Builds a static html site
- `test`: Executes configured tests/linters
- `markdownlint`: Runs [markdownlint-cli][markdownlint-cli] to validate docs are written using standard markdown format
- `textlint`: Runs [textlint][textlint] to validate docs are confirming to a set of good writing standard such as puncuation, grammer, valid links, etc.
- `textlintWithAutofix`: Runs the same validation check as `textlint` but applies fixes where possible
- `textlintWriteGood`: Scans documentation for good writing practices
- `mdspell`: Checks for spelling mistakes in documents
- `lint`: Executes all of the linters available in this repository


[doc-site]: https://documentation.elasticpath.com/template-site
[docusaurus-site]: https://docusaurus.io
[markdownlint-cli]: https://github.com/igorshubovych/markdownlint-cli
[textlint]: https://textlint.github.io
